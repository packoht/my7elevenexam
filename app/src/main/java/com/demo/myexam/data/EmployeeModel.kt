package com.demo.myexam.data

data class EmployeeModel(
    val name: String,
    val salary: String,
    val position: String
)
