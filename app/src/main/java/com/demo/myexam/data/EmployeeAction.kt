package com.demo.myexam.data

sealed interface EmployeeAction {
    data class ShowList(
        val listEmployeeModel: List<EmployeeModel>
    ) : EmployeeAction
}